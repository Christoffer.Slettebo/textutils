package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			int extra = Math.abs(width-text.length());
			return " ".repeat(extra) + text;
		}

		public String flushLeft(String text, int width) {
			int extra = Math.abs(width-text.length());
			return  text + " ".repeat(extra);
		}

		public String justify(String text, int width) {
			String[] words = text.split("\\s+");
			int extra = (width - text.length()+words.length) / (words.length-1);
			String newText = "";
			for (int i=0; i<words.length; i++){
				newText += words[i];
				if(i==words.length-1) break;
				newText += " ".repeat(extra);
			}
			return newText;
		}};
		
	@Test
	void test() {
		assertEquals("ABC", aligner.center("ABC", 4));
		assertEquals("ABCDE", aligner.center("ABCDE", 4));
		assertEquals("1  2  3", aligner.justify("1 2 3", 7));
		assertEquals("1   2   3", aligner.justify("1 2 3", 9));
		assertEquals("12   23   4", aligner.justify("12 23 4", 11));
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));

	}
}
